# syntax = docker/dockerfile:experimental

ARG PYTHON_DOCKER_TAG

FROM python:${PYTHON_DOCKER_TAG} as base

ENV DEBIAN_FRONTEND=noninteractive \
    LC_ALL=C.UTF-8 \
    LANG=C.UTF-8 \
    PYTHONDONTWRITEBYTECODE=1 \
    PATH="/opt/venv/bin:$PATH"

FROM base as builder

RUN apt-get update && \
    apt-get install -y --no-install-recommends python-dev

RUN --mount=type=bind,target=./pyproject.toml,src=./pyproject.toml \
    --mount=type=bind,target=./poetry.lock,src=./poetry.lock \
    --mount=type=cache,target=/root/.cache/pip \
    python -m venv /opt/venv && \
    pip3 install --upgrade pip && \
    pip3 install poetry && \
    poetry export > ./requirements.txt && \
    pip3 install -r ./requirements.txt

FROM base

ARG WORKDIR=/app

ARG UID=1000
ARG GID=1000

COPY --from=builder /opt/venv/ /opt/venv/

RUN mkdir /app && \
    useradd -d /app -r -u ${UID} app && \
    chown app:${GID} -R /app && \
    mkdir -p "$WORKDIR"

USER app:$GID
WORKDIR "$WORKDIR"

COPY ./entrypoint.sh ./entrypoint.sh

ENTRYPOINT ["/app/entrypoint.sh"]
