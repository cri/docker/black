#! /usr/bin/env bash

set -xeuo pipefail

black --check --diff $(find -name \*.py) | tee black.diff
